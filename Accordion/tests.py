from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class UnitTestLab7(TestCase):
    def test_apakah_website_berjalan_dan_dapat_diakses(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_apakah_website_memanggil_fungsi_index_di_views(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_apa_website_menggunakan_html_Accordion(self):
        response = Client().get('//')
        self.assertTemplateUsed(response, 'Accordion.html')

    def test_apakah_ada_tulisan_Hilmi_Arista(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Hilmi Arista", content)

    def test_apakah_ada_tulisan_about_me(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("About Me", content) 

    def test_apakah_ada_tulisan_untuk_mengubah_tema(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Theme", content) 

    def test_apakah_ada_java_script(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("<script", content) 

    def test_apakah_ada_tulisan_jquery(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js", content)

    def test_apakah_ada_button_lightmode(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("<button", content) 
        self.assertIn("Light Mode", content) 

    def test_apakah_ada_button_darkmode(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("<button", content) 
        self.assertIn("Dark Mode", content)   

    def test_apakah_ada_tulisan_activity(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Activity", content)

    def test_apakah_ada_isi_dalam_activity(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Saya sedang menjalani semester 3 di jurusan Sistem Informasi, Fakultas Ilmu Komputer, Universitas Indonesia", content)  
        self.assertIn("Saya sedang menjadi Wakil Penanggung Jawab Divisi Mentor dan Peserta Bimbingan Belajar Gratis 2020 yang diadakan oleh BEM Pengabdian Masyarakat Fasilkom UI", content) 

    def test_apakah_ada_tulisan_experience(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Experience", content)   

    def test_apakah_ada_tulisan_di_dalam_experience(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Di awal tahun 2019, saya merupakan bagian dari staf MenPes BETIS 2019", content) 
        self.assertIn("Di pertengahan tahun 2019, saya menjadi staf dari Divisi Food and Beverages COMPFEST 2019", content) 

    def test_apakah_ada_tulisan_achievement(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Achievement", content) 

    def test_apakah_ada_tulisan_di_dalam_achievement(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Juara 3 lomba Bahasa Inggris se-Jakarta Selatan", content) 
        self.assertIn("Juara 1 lomba Matematika se-Jakarta Selatan", content) 
        self.assertIn("Tiduer 24 jam tanpa jeda", content) 
        
class FunctionalTestLab7(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument("--disable-dev-shm-usage")
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTestLab7, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestLab7, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        selenium.implicitly_wait(30)
        #cari elemen
        dark = selenium.find_element_by_id('dark-btn')
        light = selenium.find_element_by_id('light-btn')
        aktivitas = selenium.find_element_by_id('aktivitas')
        pengalaman = selenium.find_element_by_id('pengalaman')
        prestasi = selenium.find_element_by_id('prestasi')
        # klik tombol tema dark
        selenium.implicitly_wait(10)
        dark.click()
        # klik tombol tema light
        selenium.implicitly_wait(10)
        light.click()
